# backend for storing the remote state of this module
terraform {
  required_version = ">= 0.9.0"

  backend "s3" {
    bucket     = "Masterthesis-2017-pipeline"
    key        = "staging/networking/terraform.tfstate"
    region     = "eu-central-1"
  }
}

# should be sourced via S3 or git eventually
module "network" {
  source = "git::https://gitlab.com/aris4/Infrastructure-module.git//modules/networking?ref=v0.7.1"
  aws_region                = "${var.aws_region}"
  resource_default_tags = ["${var.resource_default_tags}"]
}

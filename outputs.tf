output "vpc_id" {
  value = "${module.network.vpc_id}"
}

output "private_subnet_ids" {
  value = ["${module.network.private_subnet_ids}"]
}

output "availabilty_zones" {
  value = ["${module.network.availabilty_zones}"]
}

output "public_subnet_ids" {
  value = ["${module.network.public_subnet_ids}"]
}

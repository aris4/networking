variable aws_region {
  type    = "string"
  default = "eu-central-1"
}

variable resource_default_tags {
  type = "map"

  default = {
    application       = "infrastructure"
    owner             = "bienvenue-defo.de@capgemini.com"
    project           = "masterarbeit"
    terraform_managed = "true"
  }
}
